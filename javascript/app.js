/*
//= require angular.min.js
//= require_self
*/

/*(function() {
    angular.module('resume', [])
        .controller("resumeController", ["$scope", "$document", function($scope, $document){*/
    var Application = function() {
        var $ctrl = this;

        this.curSlideNum = 0;
        this.isScrolling = false;
        this.isMapInitialised = false;
        this.smallDevice = false;

        this.debug = 0;

        this.init = function () {
            // Init JS
            $("[data-toggle='tooltip']").tooltip();

            // Detect device and prepare content if small
            $ctrl.smallDevice = $(document).width() <= 786;
            if($ctrl.smallDevice){
                $("[class*='skill-slide']").addClass("active");
                $("[class*='exp-slide']").addClass("active");
            }

            // Load slide from link
            $ctrl.gotoSlide(window.location.hash, 0, 0);

            // Change slide
            $("a.scroll-link").on("click", function () {
                var target = $(this);
                var horizontalDirection = target.attr("data-horizontal-direction");
                var verticalDirection = target.attr("data-vertical-direction");
                var slide = target.attr("href");
                $ctrl.gotoSlide(slide, horizontalDirection, verticalDirection);
            });

            // Load slide when use "back/forward" button
            window.onpopstate = function () {
                var slideNum = window.location.hash;
                slideNum = slideNum.length > 0 ? slideNum : "home";
                $ctrl.gotoSlide(slideNum, 0, 1);
            };

            // Tabs for skills and experience
            $(".skill-selector").on("click mouseover", function () {
                $ctrl.gotoSubSlide(this, "skill");
            });
            $(".exp-selector").on("click mouseover", function () {
                $ctrl.gotoSubSlide(this, "exp");
            });

            // Mobile Navigation fix
            $("#link-slide-header").on("click", function (event) {
                $ctrl.onNavDropdownClick(event);
            });

            // Flying background
            var delay = $ctrl.shiftBackground($(".a-background"), [
                "images/shift1.png"
            ]);

            setTimeout(function(){
                $ctrl.shiftBackground($(".a-background2"), [
                    "images/shift2.jpg"
                ]);
            }, delay);

            // Projects popup
            $("#projectModal").on('show.bs.modal', function (event) {
                $ctrl.projectPopup(event, this);
            });
        };

        this.gotoSlide = function (slide, horizontalDirection, verticalDirection) {
            horizontalDirection = horizontalDirection ? horizontalDirection : 0;
            verticalDirection = verticalDirection ? verticalDirection : 0;
            slide = slide.length > 1 ? slide.replace("#", "") : "home";
            console.log(slide);

            if (!$ctrl.isScrolling && slide != $ctrl.curSlideNum) {
                var curSlide = $('#' + $ctrl.curSlideNum.toString());
                var newSlide = $('#' + slide.toString());

                if (newSlide.length == 1) {
                    var curSlideAnimParams = {};
                    var newSlideAnimParams = {};
                    $ctrl.curSlideNum = slide;
                    $ctrl.isScrolling = true;
                    $ctrl.updateNav(slide);

                    if (horizontalDirection != 0) {
                        newSlide.height('100vh').width('0px');
                        curSlideAnimParams.width = '0px';
                        newSlideAnimParams.width = '100vw';
                        newSlideAnimParams.marginLeft = '0px';

                        if (horizontalDirection == 1) {
                            curSlideAnimParams.marginLeft = '100vw';
                        } else {
                            newSlide.css("marginLeft", '100vw')
                        }
                    }

                    if (verticalDirection != 0) {
                        newSlide.height('0px').width('100vw');
                        curSlideAnimParams.height = '0px';
                        newSlideAnimParams.height = '100vh';
                        newSlideAnimParams.marginTop = '0px';

                        if (verticalDirection == 1) {
                            curSlideAnimParams.marginTop = '100vh';
                        } else {
                            newSlide.css("marginTop", '100vh');
                        }
                    }

                    curSlide.animate(curSlideAnimParams, {
                        duration: 500,
                        queue: false,
                        complete: function () {
                            curSlide.hide()
                                .css("marginTop", '0px')
                                .css("marginLeft", '0px');
                        }
                    });
                    newSlide.show().animate(newSlideAnimParams, {
                        duration: 500,
                        queue: false,
                        complete: function () {
                            $ctrl.isScrolling = false;
                            newSlide.width('100vw')
                                .height('100vh')
                                .css("marginTop", '0px')
                                .css("marginLeft", '0px');
                            if (slide == "contact") {
                                $ctrl.initMap();
                            }
                            window.location.href = '#' + slide;
                        }
                    });
                }
            }
        };

        this.updateNav = function(slide){
            $('a[id^="link-slide-"]').removeClass('active');
            $('#link-slide-' + slide.toString()).addClass('active');
            var dropdown = $('#link-slide-group:has(#link-slide-' + slide.toString() + ')');
            if (dropdown) {
                var link = dropdown.find('#link-slide-header');
                if (link) {
                    link.addClass('active');
                }
            }
            if (!$(".navbar-toggle").hasClass("collapsed")) {
                $(".navbar-collapse").collapse("hide");
                $(".nav-dropdown").removeClass("nav-dropdown-open");
            }
        };

        this.onNavDropdownClick = function (event) {
            if ($ctrl.smallDevice) {
                $(".nav .dropdown .nav-dropdown").toggleClass("nav-dropdown-open");
            } else {
                $ctrl.gotoSlide("overview", 0, -1)
            }
        };

        this.initMap = function () {
            if (!$ctrl.isMapInitialised) {
                var map = new google.maps.Map($("#map_canvas")[0], {
                    center: {lat: 42.98, lng: -81.244293},
                    zoom: 12,
                    mapTypeId: google.maps.MapTypeId.HYBRID,
                    disableDefaultUI: true
                });
                var map_contents = $(".map_contents");
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(map_contents[0]);
                $ctrl.isMapInitialised = true;
            }
        };

        this.gotoSubSlide = function (_target, prefix) {
            var target = $(_target);
            var slide = target.attr("data-target");
            if(slide){
                $("."+prefix+"-selector").removeClass("active");
                target.addClass("active");
                $("[class*='"+prefix+"-slide']")
                    .removeClass("active")
                    .filter("." + slide)
                    .addClass("active");
            }
        };

        this.shiftBackground = function(target, slides, duration, subDuration, i){
            if(!slides){
                return;
            }
            i = i ? i : 0;
            duration = duration ? duration : 12000;
            subDuration = subDuration ? subDuration : 5000;
            var delay1 = duration - subDuration;
            var delay2 = delay1 - subDuration - 0;
            slides = $.isArray(slides) ? slides : [slides];

            setTimeout(function(){
                target
                    .fadeTo(subDuration, 0, function(){
                })
            }, delay1);

            var z = target.css("z-index") - 1;

            target
                .css("background-position-x", 0)
                .css("background-position-y", 0)
                .css("opacity", 1)
                .css("background-image", "linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), none, none, url('"+slides[i]+"')")
                .css("z-index", z)
                .animate(
                    {
                        'background-position-x': "-400px",
                        'background-position-y': "-400px"
                    }, {
                        duration: duration,
                        queue: false,
                        complete: function(){
                            target.stop(true);
                            setTimeout(function(){
                                $ctrl.shiftBackground(target, slides, duration, subDuration, (slides && slides.length > i + 1 ? i + 1 : 0));
                            }, delay2);
                        }
                    }
                );

            return delay1;
        };

        this.projectPopup = function(event, target){
            var button = $(event.relatedTarget);
            var type = button.data('type');
            var size = button.data('size');
            var title = button.data('title');

            var modal = $(target);
            modal.find(".modal-dialog").removeClass("modal-lg modal-md").addClass("modal-"+size);
            modal.find('[id^="carousel-content"]').hide().find("div").removeClass("item active");
            var firstSlide = modal.find('#carousel-content-'+type).show().find("div").addClass("item")[0];
            $(firstSlide).addClass("active");
            $("#projectModalLabel").html(title);
        }
    };
    var app = new Application();
//})();